package crawlerTools;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {

    public static String regexSearch(String str, String pattern) {
        return regexSearch(str, pattern, 0);
    }
    public static String regexSearch(String str, String pattern, int index) {
        Pattern r = Pattern.compile(pattern);
        Matcher mat = r.matcher(str);
        if (mat.find()) {
            return mat.group(index);
        } else {
            return null;
        }
    }
}