package crawlerTools;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JsonMap {
    private Object oJson;

    public JsonMap() {
    }

    public JsonMap(Object jStr) {
        this.oJson = jStr;
    }

    public static Map<Object, Object> jsonToMap(Object jsonObj) {
        JSONObject jsonObject = JSONObject.fromObject(jsonObj);
        Map<Object, Object> map = (Map) jsonObject;
        return map;
    }

    public static void main(String[] args) {
        String content = "{id={\"k\":123}, catid=28, typeid=0, title=智慧赋能，展黄河生态文明最美画卷, style=, thumb=https://img.jinantimes.com.cn/67548f892aa461c53a5c450786213249.jpg?w=224&h=150, keywords=, description=, posids=0, url=https://api.jinantimes.com.cn/h5/content.html?catid=28&id=1889509, listorder=0, status=99, sysadd=1, islink=0, username=孟祥君, inputtime=2小时前, updatetime=1小时前, plsum=0, ding=11, cai=0, playtype=1, type=1, place=1, display=1, location=, userid=0, views=153, favorites=0, trueviews=0, author=, checker=李玉王, checktime=1639790294, editor=, is_del=0, start=0, end=0, score=, peoplenum=, auditor=, label=, reporterid=0, bltel=0, reporter=, diylabel=, es_status=0, lbcolor=#cccccc, picauthor=, article_class=0, passtime=0, is_original=1, video_url=0, src_type=1, src_id=0, special_id=0, videourl=, src_weixinid=, istop=0, gwsb=, oldid=0, oldcatid=0, is_share=1, shorttitle=, authors=罗晓飞/记者/true,王菲/校对/true,孟祥君/编辑/true,黄中明/摄影/true,, heima=1, wordCount=1383, haibao=, lastid=1889533, ztcatid=0, catname=新动能, taidu=11, thumbs=[{\\\"url\\\":\\\"https:\\\\/\\\\/img.jinantimes.com.cn\\\\/67548f892aa461c53a5c450786213249.jpg\\\",\\\"alt\\\":\\\"979\\\"},{\\\"url\\\":\\\"https:\\\\/\\\\/img.jinantimes.com.cn\\\\/878e165279ac5531ca6b7ce658d08a54.jpg\\\",\\\"alt\\\":\\\"6266\\\"},{\\\"url\\\":\\\"https:\\\\/\\\\/img.jinantimes.com.cn\\\\/e0674fc0470991cda57c39a47695483d.jpg\\\",\\\"alt\\\":\\\"979\\\"},{\\\"url\\\":\\\"https:\\\\/\\\\/img.jinantimes.com.cn\\\\/878e165279ac5531ca6b7ce658d08a54.jpg\\\",\\\"alt\\\":\\\"6266\\\"}], base_userid=20, iscell=0, isPraised=0, shareurl=https://api.jinantimes.com.cn/h5/content.html?catid=28&id=1889509&fx=1, shareUrl=https://api.jinantimes.com.cn/h5/content.html?catid=28&id=1889509&fx=1, avatar=https://api.jinantimes.com.cn/uploadfile/avatar/default.png?1639796331, copyfrom=新黄河, dylist=[], mediatype=0}";
        Map<Object, Object> map = jsonPathItemtoMap(content);
    }

    public static Map<Object, Object> jsonPathItemtoMap(String jsonPathItem) {
        Map<Object, Object> map = new HashMap();
        try {
            String text = RegexUtil.regexSearch(jsonPathItem, "\\{(.+)\\}", 1);
            String[] fieldList = text.split(",");
            for (String field : fieldList) {
                String[] item = field.trim().split("=");
                if (item.length < 1) continue;
                Object value = item.length < 2 ? "" : item[1];
                if (JsonMap.isJsonObject(value)) {
                    value = jsonToMap(value);
                }
                map.put(item[0], value);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return map;
    }

    public static boolean isJsonObject(Object obj) {
        try {
            JSONObject.fromObject(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void setoJson(Object oJson) {
        this.oJson = oJson;
    }

    public JsonMap get(Object s) {
        if (s instanceof Integer) return new JsonMap(JSONArray.fromObject(oJson).get((Integer) s));
        else return jsonToMap(oJson).get(s) == null ? null : new JsonMap(jsonToMap(oJson).get(s));
    }

    @Override
    public String toString() {
        return oJson.toString();
    }

    public int length() {
        if (this.oJson == null) return 0;
        else return JSONArray.fromObject(oJson).size();
    }
}
