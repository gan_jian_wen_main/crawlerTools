package crawlerTools;

import java.util.List;
import java.util.Map;

public class Response {
    private String content;
    private JsonMap json;
    private int responseCode;
    private Map<String, List<String>> headers;

    public Response(String content, int responseCode, Map<String, List<String>> headers) {
        this.content = content;
        this.json = null;
        this.responseCode = responseCode;
        this.headers = headers;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    @Override
    public String toString() {
        return "crawlerTools.Response{" +
                "content='" + content + '\'' +
                ", json=" + json +
                ", responseCode=" + responseCode +
                ", headers=" + headers +
                '}';
    }

    public String getContent() {
        return content;
    }

    public JsonMap getJson() {
        return new JsonMap(this.getContent());
    }
};